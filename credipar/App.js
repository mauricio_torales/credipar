import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Inicio from './pages/Inicio';
import Formulario from './pages/Formulario';
import Requisitos from './pages/Requisitos';

const FirstActivity_StackNavigator = createStackNavigator({
  //All the screen from the Screen1 will be indexed here
  Inicio: {
   screen: Inicio,
  },
  Formulario: {
    screen: Formulario,
   },
   Requisitos: {
    screen: Requisitos,
   },
 

});

export default createAppContainer(FirstActivity_StackNavigator);