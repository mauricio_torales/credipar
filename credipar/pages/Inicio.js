import React, { Component } from 'react';
import {TouchableOpacity,Image, StyleSheet, View, Text, Linking } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
// import all basic components
export default class Inicio extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerShown: false,
        };
      };

    render() {
        return (
            <View style={styles.MainContainer}>
                 <Image source={require("../img/logo_inicio.png")} style={styles.Logo}/>
                 <TouchableOpacity onPress={() => this.props.navigation.navigate('Formulario')} style={styles.BtnSolicitar} >
                     <Text style={styles.TxtBtn} >Solicitar credito</Text>
                 </TouchableOpacity> 
                <View style={{ margin:20, alignItems:'center'}}>
                

                </View>
                <TouchableOpacity onPress={() => Linking.openURL(`https://www.crediparelectrodomesticos.com/`)}  style={{...styles.BtnLink, marginTop:50}} >
                    <Text style={{...styles.TxtBtn, color:'#fff'}} >Ofertas de electrodomésticos</Text>
                </TouchableOpacity> 
                <TouchableOpacity onPress={() => Linking.openURL(`https://credipar.com.py/`)}  style={{...styles.BtnLink, marginTop:10}} >
                    <Text style={{...styles.TxtBtn, color:'#fff'}} >Ofertas inmobiliarias</Text>
                </TouchableOpacity> 

                 <Text style={{position: "absolute",
                    bottom:40,}} >Mira los requisitos minimos <Text onPress={() => this.props.navigation.navigate('Requisitos')}  style={{fontWeight:"bold"}}>aquí</Text>
                 </Text>

            </View>
         )
    }
}

const styles = StyleSheet.create({
    MainContainer: {
      flex: 1,
      alignItems: 'center',
    },
    Logo:{
        marginTop:100,
        width: 200,
        height:200,
        resizeMode:"contain"
    },
    BtnSolicitar:{
        width: wp('70%'),
        backgroundColor: '#FFF200',
        padding:15,
        borderRadius:15,
        position: "absolute",
        bottom:80,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,

        elevation: 6,
        
    },
    BtnLink:{
        width: wp('70%'),
        backgroundColor: '#302D5D',

        borderRadius:15,
        position: "relative",
        bottom:80,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,

        
    },
    TxtBtn:{
        textAlign: "center",
        fontSize:20,
        fontWeight:"bold"
    }

});