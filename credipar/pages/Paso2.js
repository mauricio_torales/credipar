import React, { Component } from "react";
import {TextInput, Text, StyleSheet, View, TouchableOpacity, Image, ScrollView, AsyncStorage, KeyboardAvoidingView } from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import AwesomeAlert from 'react-native-awesome-alerts';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import DatePicker from 'react-native-datepicker';
import { TextInputMask } from 'react-native-masked-text'


class paso2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAlert: false,
      empresa_trabajo:'',
      direccion_laboral:'',
      telefono_laboral:'',
      antiguedad_laboral:'',
      ingresos:'',
      egresos:'',
      date:"",
      antiguedad_laboral_meses:0,
      title:"Por favor carga todos los datos...",
      message:"No puedes dejar los espacion en blanco..."
    };
  }

  async componentDidMount(){
    this.setState({
      empresa_trabajo: await AsyncStorage.getItem('empresa_trabajo'),
      direccion_laboral: await AsyncStorage.getItem('direccion_laboral'),
      telefono_laboral: await AsyncStorage.getItem('telefono_laboral'),
      antiguedad_laboral: await AsyncStorage.getItem('antiguedad_laboral'),
      ingresos: await AsyncStorage.getItem('ingresos'),
      egresos: await AsyncStorage.getItem('egresos'),
      date: await AsyncStorage.getItem('fecha_ingreso'),
      antiguedad_laboral_meses: await AsyncStorage.getItem('antiguedad_laboral_meses')
    })
}
  
  nextStep = () => {
    if(this.state.empresa_trabajo=='' || this.state.empresa_trabajo==null || 
    this.state.direccion_laboral=='' || this.state.direccion_laboral==null || 
    this.state.telefono_laboral=='' ||  this.state.telefono_laboral==null || 
    this.state.ingresos=='' || this.state.ingresos==null || 
    this.state.date=='' || this.state.date==null || 
    this.state.egresos=='' || this.state.egresos==null){
      this.showAlert();
      return;
    }

    var current_year = new Date().getFullYear();
    var antiguedad_year = current_year-this.state.date.split('/')[2].trim()
    antiguedad_year = antiguedad_year*12;

    if(parseInt(this.state.antiguedad_laboral_meses)>antiguedad_year){
      this.setState({message:'Debe tener al menos ' +  this.state.antiguedad_laboral_meses+ ' meses laborales para solicitar un credito', title:'Atención.'})
      this.showAlert();
      return;
    }

    const { next, saveState } = this.props;
    // Save state for use in other steps
    saveState({ name: "ejemplo" });


    // Go to next step
    next();
  };

  goBack() {
    const { back } = this.props;
    // Go to previous step
    back();
  }

  async date (date){
    this.setState({date})
    await AsyncStorage.setItem('fecha_ingreso', date)
    
  }

  handleInputChange = async (inputName, inputValue) => {
    this.setState(state => ({
      ...state, 
      [inputName]: inputValue
    }));
    inputValue = inputValue.replace("Gs ", "");
    inputValue = inputValue.replace(".", "");
    inputValue = inputValue.replace(".", "");
    await AsyncStorage.setItem(inputName, inputValue)
  }

  showAlert = () => {
    this.setState({
      showAlert: true
    });
  };

  hideAlert = () => {
    this.setState({
      showAlert: false
    });
  };

  render() {
    const {showAlert} = this.state;

    return (
      <KeyboardAvoidingView
      behavior={Platform.OS == "ios" ? "padding" : "height"}
      style={styles.container}
    >
      
      <ScrollView contentContainerStyle={styles.Main}>
        <View style={styles.Cabecera} >
            <Image source={require("../img/logo-2.png")} style={styles.Logo}/>
            <Image source={require("../img/figura.png")} style={styles.Figura}/>
        </View>
        <View>
        <Text style={styles.TxtNext}>Datos Laborales:</Text>        
        <TextInput value={this.state.empresa_trabajo} name="empresa_trabajo" onChangeText={value => this.handleInputChange('empresa_trabajo', value)} placeholderTextColor="#333" placeholder='Entidad donde trabaja' style={styles.formulario} />
        <TextInput value={this.state.direccion_laboral} name="direccion_laboral" onChangeText={value => this.handleInputChange('direccion_laboral', value)} placeholderTextColor="#333" placeholder='Dirección de trabajo' style={styles.formulario} />
          <TextInput value={this.state.telefono_laboral} name="telefono_laboral" onChangeText={value => this.handleInputChange('telefono_laboral', value)} placeholderTextColor="#333" placeholder='Teléfono laboral' style={styles.formulario} />
                <DatePicker
                style={{width: 'auto',
                  borderWidth: 0.5,
                  borderRadius:15,
                  borderColor: '#FFFFFF',
                  color:'#FFFFFF',
                  backgroundColor:'#FFFFFF',
                  marginTop:10,
                  marginBottom:10,
                  paddingLeft:10
                }}
                date={this.state.date}
                mode="date"
                placeholder="Fecha de ingreso"
                placeholderTextColor="#FFFFFF"
                format="DD/MM/YYYY"
                minDate="01-01-1950"
                confirmBtnText="Confirmar"
                cancelBtnText="Cancelar"
                customStyles={{
                  
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0
                  },
                  dateInput: {
                    marginLeft: 6,
                    borderWidth: 0
                  }
                 
                }}
                onDateChange={(date) => {this.date(date)}}
              />
          <TextInputMask
            type={'money'}
            style={styles.formulario}
            name="ingresos"
            onChangeText={value => this.handleInputChange('ingresos', value)} 
            placeholderTextColor="#333" 
            placeholder='Ingresos'
            options={
              {
                precision: 0,
              separator: '.',
              delimiter: '.',
              unit: 'Gs ',
              suffixUnit: ''
              }
            }

            // dont forget to set the "value" and "onChangeText" props
            value={this.state.ingresos}
  
          />

          <TextInputMask
            type={'money'}
            style={styles.formulario}
            name="egresos"
            onChangeText={value => this.handleInputChange('egresos', value)} 
            placeholderTextColor="#333" 
            placeholder='Egresos'
            options={
              {
                precision: 0,
              separator: '.',
              delimiter: '.',
              unit: 'Gs ',
              suffixUnit: ''
              }
            }

            // dont forget to set the "value" and "onChangeText" props
            value={this.state.egresos}
  
          />
        </View>
        <View >
            <TouchableOpacity style={styles.BtnNext}  onPress={this.nextStep}>
              <Text style={styles.TxtNext}>Siguiente</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.BtnNext}  onPress={this.props.back}>
              <Text style={styles.TxtNext}>Anterior</Text>
            </TouchableOpacity>
        </View>
        </ScrollView>
       <AwesomeAlert
            show={showAlert}
            showProgress={false}
            title={this.state.title}
            message={this.state.message}
            closeOnTouchOutside={true}
            closeOnHardwareBackPress={false}
            showCancelButton={false}
            showConfirmButton={true}
        //    cancelText="No, cancel"
            confirmText="OK!"
            confirmButtonColor="#DD6B55"
            onCancelPressed={() => {
              this.hideAlert();
            }}
            onConfirmPressed={() => {
              this.hideAlert();
            }}
          />
</KeyboardAvoidingView>
    );
  }
}

export default paso2;
const styles = StyleSheet.create({
  Main: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#302D5D'
  },
  container: {
    flex: 1
  },
  Cabecera:{
    width: wp('100%'),
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
  },
  Logo: {
    marginTop:50,
    width: 250,
    height:50,
    resizeMode:"contain"
  },
  Figura: {
    width: wp('100%'),
    resizeMode:"contain",
    marginBottom: -5,
    marginTop:10
  },
  BtnNext:{
    width: wp('60%'),
    borderWidth: 0.5,
    borderRadius:15,
    padding:10,
    borderColor: '#FFFFFF',
    marginTop:20
  },
  TxtNext:{
    textAlign: "center",
    fontSize:15,
    color:"#FFFFFF",
   
  },
  formulario:{
    width: wp('90%'),
    height:40,
    borderWidth: 0.5,
    borderRadius:15,
    borderColor: '#FFFFFF',
    marginTop:10,
    marginBottom:10,
    paddingLeft:10,
    color:'#000',
    backgroundColor:'#FFF'
  }
 
});