import React, { Component } from "react";
import {Platform, TextInput, Text, StyleSheet, View, TouchableOpacity, Image, AsyncStorage, KeyboardAvoidingView, Picker } from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
// import RNPickerSelect from 'react-native-picker-select';
import AwesomeAlert from 'react-native-awesome-alerts';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import Spinner from 'react-native-loading-spinner-overlay';
import { TextInputMask } from 'react-native-masked-text'
import Constants from 'expo-constants';


async function getToken() {
  // Remote notifications do not work in simulators, only on device
  
  if (Constants.isDevice) {
    const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      alert('Failed to get push token for push notification!');
      return;
    }
    let token = await Notifications.getExpoPushTokenAsync();
    await AsyncStorage.setItem('token_phone', token)
    console.log('Our token', token);
  } else {
    console.log('Must use physical device for Push Notifications');
  }

  if (Platform.OS === 'android') {
    Notifications.createChannelAndroidAsync('default', {
      name: 'General',
      sound: true,
      priority: 'max',
      vibrate: [0, 250, 250, 250],
    });
  }

  
  
}

class paso4 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token_phone:'',
      showAlert: false,
      nombre_solicitante:'',
      nro_documento:'',
      email:'',
      direccion_particular:'',
      barrio:'',
      telefono:'',

      empresa_trabajo:'',
      direccion_laboral:'',
      telefono_laboral:'',
      ingresos:'',
      egresos:'', 


      monto_solicitado:'',
      plazo_solicitado:'',
      sucursal_id:'',
      localSpinner:[],
      plazosSnipper:[],
      monto_cuota : 0,
      cantidad_salario: 0,
      porcentaje_salario : 0,
      cantidad_millones:0,
      cuotas:[],

      title:'Por favor carga todos los datos...',
      message:'No puedes dejar los espacion en blanco...',
      spinner:false, 
    };
  }

  handleNotification = ({ origin, data }) => {
    console.log(
    `Push notification ${origin} with data: ${JSON.stringify(data)}`,
    );
  };

  async componentDidMount(){
  
   //await console.log('Aca despues de posicion');
    
   await this.setState({
    locales: [],
    spinner:true
  }, async ()=>{ 
    // locales de la web
    var response_locales = await fetch('http://18.221.77.253/admin/sucursales/getSucursales')
    var responseJson_locales = await response_locales.json()
    this.setState({
      locales: responseJson_locales
    }, ()=>{
       this.state.localSpinner = this.state.locales.sucursales.map(x => ({
        label: x.nombre,
        value: x.idsucursales
      }));
    })
  });


  await this.setState({
    cuotas: [],
  }, async ()=>{ 
    // locales de la web
    var response_cuotas = await fetch('http://18.221.77.253/admin/cuoteros/getCuotas')
    var responseJson_cuotas = await response_cuotas.json()
    this.setState({
      cuotas: responseJson_cuotas
    }, ()=>{
      this.state.plazosSnipper = this.state.cuotas.cuoteros.map(x => ({
        label: "" + x.plazo + "",
        value: x.plazo
      }));
      console.log(this.state.plazosSnipper)
    })
  });

  


  await this.setState({
    parametros: [],
  }, async ()=>{ 
    // locales de la web
    var response_parametros = await fetch('http://18.221.77.253/admin/parametros/getParametros')
    var responseJson_parametros = await response_parametros.json()
    this.setState({
      parametros: responseJson_parametros
    }, ()=>{
      this.state.parametros.map((x) => {
        if(x.idparametros==2){
          this.setState({cantidad_salario:x.valor})
        }
        if(x.idparametros==3){
          this.setState({porcentaje_salario:x.valor})
        }
      });
    })
  });

    
    this._notificationSubscription = Notifications.addListener(this._handleNotification);

    this.setState({
      token_phone: await AsyncStorage.getItem('token_phone'),

      nombre_solicitante: await AsyncStorage.getItem('nombre_solicitante'),
      nro_documento: await AsyncStorage.getItem('nro_documento'),
      email: await AsyncStorage.getItem('email'),
      direccion_particular: await AsyncStorage.getItem('direccion_particular'),
      barrio: await AsyncStorage.getItem('barrio'),
      telefono: await AsyncStorage.getItem('telefono'),

      empresa_trabajo: await AsyncStorage.getItem('empresa_trabajo'),
      direccion_laboral: await AsyncStorage.getItem('direccion_laboral'),
      telefono_laboral: await AsyncStorage.getItem('telefono_laboral'),
      ingresos: await AsyncStorage.getItem('ingresos'),
      egresos: await AsyncStorage.getItem('egresos'),

      monto_solicitado: await AsyncStorage.getItem('monto_solicitado'),
      fecha_nacimiento: await AsyncStorage.getItem('fecha_nacimiento'),
      fecha_ingreso: await AsyncStorage.getItem('fecha_ingreso'),
      estado_civil: await AsyncStorage.getItem('estado_civil'),
  
    },()=>{
      this.setState({
        cantidad_millones : this.state.monto_solicitado/1000000,
        spinner:true
      }, async ()=>{
        setTimeout(async()=> {
          await  getToken()
                  .then(this.setState({spinner:false, token_phone:await AsyncStorage.getItem('token_phone')}));
          }, 1500);
      })
      
    });

}

_createNotificationAsync = () => {
  Expo.Notifications.presentLocalNotificationAsync({
    title: 'General',
    body: 'Notificacion Credipar',
    android: {
      channelId: 'general',
      color: '#FF0000',
    },
  });
}

_handleNotification = (notification) => {
  this.setState({notification: notification});
};

  nextStep = () => {
    if(this.state.monto_solicitado=='' || this.state.monto_solicitado==null || 
    this.state.plazo_solicitado=='' || this.state.plazo_solicitado==null || 
    this.state.sucursal_id=='' ||  this.state.sucursal_id==null){
      this.showAlert();
      return;
    }
    const { next, saveState } = this.props;
    // Save state for use in other steps
    saveState({ name: "ejemplo" });

    // Go to next step
    next();
  };

  goBack() {
    const { back } = this.props;
    // Go to previous step
    back();
  }

  handleInputChange = async (inputName, inputValue) => {
    this.setState(state => ({
      ...state, 
      [inputName]: inputValue
    }));
    inputValue = inputValue.replace("Gs ", "");
    inputValue = inputValue.replace(".", "");
    inputValue = inputValue.replace(".", "");

    await AsyncStorage.setItem(inputName, inputValue)
    if(inputName=='monto_solicitado'){
      this.setState({
        cantidad_millones : parseInt(inputValue)/1000000,
        plazo_solicitado : '',
        monto_cuota : 0
      },()=>{
      })
    }
  }

  showAlert = () => {
    this.setState({
      showAlert: true
    });
  };

  hideAlert = () => {
    this.setState({
      showAlert: false
    });
  };

  async procesar(){
    
     this.setState({spinner:true})

    if(this.state.token_phone=='' || this.state.token_phone==null){
      await getToken();
      await this.setState({token_phone:await AsyncStorage.getItem('token_phone')});
    }

    if(this.state.sucursal_id==null || this.state.plazo_solicitado=='' || this.state.sucursal_id=='' || this.state.plazo_solicitado==null || this.state.monto_solicitado=='' || this.state.monto_solicitado==null){
      this.showAlert();
      this.setState({spinner:false})
      return;
    }

    if(this.state.monto_solicitado > (this.state.ingresos * this.state.cantidad_salario) ){
      this.setState({message:'El monto solicitado supera más de ' + this.state.cantidad_salario +' veces su salario', title:'Atención.'})
      this.setState({spinner:false})
      await AsyncStorage.setItem('estadoPedido', 'R')
      this.insertaRechazados();
      this.showAlert();
      return;
    }

    if( (this.state.monto_cuota > this.state.ingresos * this.state.porcentaje_salario/100) ){
      this.setState({message:'El monto de la cuota supera el' + this.state.porcentaje_salario +'% de su salario', title:'Atención.'})
      this.setState({spinner:false})
      await AsyncStorage.setItem('estadoPedido', 'R')
      this.insertaRechazados();
      this.showAlert();
      return;
    }


      console.log(JSON.stringify({
      Solicitudes:{ 
        nombre_solicitante : this.state.nombre_solicitante,
        nro_documento : this.state.nro_documento,  
        email : this.state.email,  
        direccion_particular : this.state.direccion_particular,  
        barrio : this.state.barrio,  
        telefono : this.state.telefono,  
        empresa_trabajo : this.state.empresa_trabajo,  
        direccion_laboral : this.state.direccion_laboral,  
        telefono_laboral : this.state.telefono_laboral,    
        ingresos : this.state.ingresos,  
        egresos : this.state.egresos,      
        sucursal_id : this.state.sucursal_id,
        token_phone : this.state.token_phone,
        cuotero_id : this.state.cuotero_id,
        fecha_nacimiento : this.state.fecha_nacimiento,
        fecha_ingreso : this.state.fecha_ingreso,
        monto_solicitado : this.state.monto_solicitado,
        plazo_solicitado : this.state.plazo_solicitado,
        estado_civil : this.state.estado_civil
        }
      }))

    try {

      await AsyncStorage.setItem('estadoPedido', 'P')

      const response = await fetch('http://18.221.77.253/admin/solicitudes/insertSolicitud', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
        body: JSON.stringify({
          Solicitudes:{ 
              nombre_solicitante : this.state.nombre_solicitante,
              nro_documento : this.state.nro_documento,  
              email : this.state.email,  
              direccion_particular : this.state.direccion_particular,  
              barrio : this.state.barrio,  
              telefono : this.state.telefono,  
              empresa_trabajo : this.state.empresa_trabajo,  
              direccion_laboral : this.state.direccion_laboral,  
              telefono_laboral : this.state.telefono_laboral,    
              ingresos : this.state.ingresos,  
              egresos : this.state.egresos,      
              sucursal_id : this.state.sucursal_id,
              token_phone : this.state.token_phone,
              cuotero_id : this.state.cuotero_id,
              fecha_nacimiento : this.state.fecha_nacimiento,
              fecha_ingreso : this.state.fecha_ingreso,
              monto_solicitado : this.state.monto_solicitado,
              plazo_solicitado : this.state.plazo_solicitado,
              estado_civil : this.state.estado_civil

            }
          }),
      });

      

      const responseJson = await response.json();
      console.log(responseJson)
      if(responseJson != null){
        if(responseJson.response){
          this.setState({spinner:false})
         this.nextStep();
        }else{
          this.setState({spinner:false,message:'La solicitud no se pudo procesar.', title:'Por favor intente de nuevo.'})
          this.showAlert()
        }

       
      }
      
    }
    catch (error) {
      this.setState({spinner:false})
      console.error(error);
    }
    
  }

  async insertaRechazados(){
    try {

      var date = new Date().getDate();
      var month = new Date().getMonth() + 1;
      var year = new Date().getFullYear();

      //Alert.alert(date + '-' + month + '-' + year);
      // You can turn it in to your desired format
      var fecha = year + '-' + month + '-' + date;//format: dd-mm-yyyy;
      
      const response = await fetch('http://18.221.77.253/admin/solicitudes/insertSolicitud', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
        body: JSON.stringify({
          Solicitudes:{ 
              nombre_solicitante : this.state.nombre_solicitante,
              nro_documento : this.state.nro_documento,  
              email : this.state.email,  
              direccion_particular : this.state.direccion_particular,  
              barrio : this.state.barrio,  
              telefono : this.state.telefono,  
              empresa_trabajo : this.state.empresa_trabajo,  
              direccion_laboral : this.state.direccion_laboral,  
              telefono_laboral : this.state.telefono_laboral,    
              ingresos : this.state.ingresos,  
              egresos : this.state.egresos,      
              sucursal_id : this.state.sucursal_id,
              token_phone : this.state.token_phone,
              cuotero_id : this.state.cuotero_id,
              fecha_nacimiento : this.state.fecha_nacimiento,
              fecha_ingreso : this.state.fecha_ingreso,
              monto_solicitado : this.state.monto_solicitado,
              plazo_solicitado : this.state.plazo_solicitado,
              estado_civil : this.state.estado_civil,
              estado:'R',
              motivo_rechazo: this.state.message,
              fecha_rechazo: fecha

            }
          }),
      });

      

      const responseJson = await response.json();
      console.log(responseJson)
      if(responseJson != null){
        if(responseJson.response){
          this.setState({spinner:false})
         this.nextStep();
        }else{
          this.setState({spinner:false,message:'La solicitud no se pudo procesar.', title:'Por favor intente de nuevo.'})
          this.showAlert()
        }
      }

      console.log(JSON.stringify({
        Solicitudes:{ 
            nombre_solicitante : this.state.nombre_solicitante,
            nro_documento : this.state.nro_documento,  
            email : this.state.email,  
            direccion_particular : this.state.direccion_particular,  
            barrio : this.state.barrio,  
            telefono : this.state.telefono,  
            empresa_trabajo : this.state.empresa_trabajo,  
            direccion_laboral : this.state.direccion_laboral,  
            telefono_laboral : this.state.telefono_laboral,    
            ingresos : this.state.ingresos,  
            egresos : this.state.egresos,      
            sucursal_id : this.state.sucursal_id,
            token_phone : this.state.token_phone,
            cuotero_id : this.state.cuotero_id,
            fecha_nacimiento : this.state.fecha_nacimiento,
            fecha_ingreso : this.state.fecha_ingreso,
            monto_solicitado : this.state.monto_solicitado,
            plazo_solicitado : this.state.plazo_solicitado,
            estado_civil : this.state.estado_civil,
            estado:'R',
            motivo_rechazo: this.state.message,
            fecha_rechazo: fecha

          }
        }))
      
    }
    catch (error) {
      this.setState({spinner:false})
      console.error(error);
    }
  }

  async snipper(inputName, inputValue){
    this.setState(state => ({
      ...state, 
      [inputName]: inputValue
    }));

    if(inputName=='plazo_solicitado'){
     // this.state.cuotas.cuoteros.map((x) => x.plazo==inputValue? this.state.monto_cuota=x.monto*this.state.cantidad_millones:this.state.monto_cuota=0 );
      this.state.cuotas.cuoteros.map((x) => {
       if(x.plazo==inputValue){
        this.state.monto_cuota=x.monto*this.state.cantidad_millones
       }
      });
    }
    //await AsyncStorage.setItem(inputName, inputValue)
  }


  formatNumber(num) {
    if (!num || num == 'NaN') return '-';
    if (num == 'Infinity') return '&#x221e;';
    num = num.toString().replace(/\$|\,/g, '');
    if (isNaN(num))
        num = "0";
    var sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * 100 + 0.50000000001);
    num = Math.floor(num / 100).toString();
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3) ; i++)
        num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
    return (((sign) ? '' : '-') + num );
}

  render() {
    const {showAlert} = this.state;

    return (
      <KeyboardAvoidingView style={styles.Main} >
        <Spinner
          visible={this.state.spinner}
          textContent={'Cargando...'}
          textStyle={styles.spinnerTextStyle}
        />
        <View style={styles.Cabecera} >
            <Image source={require("../img/logo-2.png")} style={styles.Logo}/>
            <Image source={require("../img/figura.png")} style={styles.Figura}/>
        </View>
        <View>
        <Text style={styles.TxtNext}>Solicitud de credito:</Text>

        
        <TextInputMask
            type={'money'}
            style={styles.formulario}
            name="monto_solicitado"
            onChangeText={value => this.handleInputChange('monto_solicitado', value)} 
            placeholderTextColor="#333" 
            placeholder='Solicita la suma de GS...'
            options={
              {
                precision: 0,
              separator: '.',
              delimiter: '.',
              unit: 'Gs ',
              suffixUnit: ''
              }
            }

            // dont forget to set the "value" and "onChangeText" props
            value={this.state.monto_solicitado}
  
          />

        {/* <RNPickerSelect 
              placeholder={{
                label: 'Plazo'
            }}
            value={this.state.plazo_solicitado}
            onValueChange={(value) => this.snipper('plazo_solicitado', value)}
            items={this.state.plazosSnipper}
        /> */}
          <View style={{borderRadius: 15, borderWidth: 1, borderColor: '#fff', overflow: 'hidden', marginTop:10, marginBottom:10}}>
            <Picker
                selectedValue={this.state.plazo_solicitado}
                onValueChange={(value) => this.snipper('plazo_solicitado', value)}
                style={{ width: 'auto', color:'#fff', height: 35 }}
                mode="dropdown">
                <Picker.Item label="Cant. de Cuotas" value="" />

                { this.state.plazosSnipper.map((item, key)=>
                  <Picker.Item label={item.label} value={item.value} key={key} />
                )}
                
              </Picker>
          </View>
          
          {/*<RNPickerSelect 
              placeholder={{
                label: 'Cuotas por millón'
            }}
            value={this.state.cuotero_id}
            onValueChange={(value) => this.snipper('cuotero_id', value)}
            items={this.state.cuotasSpinner}
          />*/}


          {/* <RNPickerSelect 
              placeholder={{
                label: 'Sucursal más cercana..'
            }}
            value={this.state.sucursal_id}
            onValueChange={(value) => this.snipper('sucursal_id', value)}
            items={this.state.localSpinner}
        /> */}

      <View style={{borderRadius: 15, borderWidth: 1, borderColor: '#fff', overflow: 'hidden', marginTop:10, marginBottom:10}}>
            <Picker
                selectedValue={this.state.sucursal_id}
                onValueChange={(value) => this.snipper('sucursal_id', value)}
                style={{ width: 'auto', color:'#fff', height: 35 }}
                mode="dropdown">
                <Picker.Item label="Seleccione Sucursal" value="" />

                { this.state.localSpinner.map((item, key)=>
                  <Picker.Item label={item.label} value={item.value} key={key} />
                )}
                
              </Picker>
          </View>


        </View>
        <View >
            <TouchableOpacity style={styles.BtnNext}  onPress={this.props.back}>
              <Text style={styles.TxtNext}>Anterior</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.BtnSol} onPress={(event)=>this.procesar()}>
              <Text style={styles.TxtNext}>Solicitar</Text>
            </TouchableOpacity>
        </View>
        <AwesomeAlert
            show={showAlert}
            showProgress={false}
            title={this.state.title}
            message={this.state.message}
            closeOnTouchOutside={true}
            closeOnHardwareBackPress={false}
            showCancelButton={false}
            showConfirmButton={true}
        //    cancelText="No, cancel"
            confirmText="OK!"
            confirmButtonColor="#DD6B55"
            onCancelPressed={() => {
              this.hideAlert();
            }}
            onConfirmPressed={() => {
              this.hideAlert();
            }}
          />
      </KeyboardAvoidingView>
    );
  }
}

export default paso4;
const styles = StyleSheet.create({
  Main: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#302D5D'
  },
  Cabecera:{
    width: wp('100%'),
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
  },
  Logo: {
    marginTop:50,
    width: 250,
    height:50,
    resizeMode:"contain"
  },
  Figura: {
    width: wp('100%'),
    resizeMode:"contain",
    marginBottom: -5,
    marginTop:10
  },
  BtnNext:{
    width: wp('60%'),
    borderWidth: 0.5,
    borderRadius:15,
    padding:10,
    borderColor: '#FFFFFF',
    marginTop:20
  },
  TxtNext:{
    textAlign: "center",
    fontSize:15,
    color:"#FFFFFF",
   
  },
  formulario:{
    width: wp('90%'),
    height:40,
    borderWidth: 0.5,
    borderRadius:15,
    borderColor: '#FFFFFF',
    marginTop:10,
    marginBottom:10,
    paddingLeft:10,
    color:'#000',
    backgroundColor:'#FFF'
  },
  BtnSol:{
    width: wp('60%'),
    borderWidth: 0.5,
    borderRadius:15,
    padding:10,
    borderColor: '#0B6623',
    backgroundColor:'#0B6623',
    marginTop:50
  }
 
});