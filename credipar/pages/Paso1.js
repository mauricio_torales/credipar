import React, { Component } from "react";
import {TextInput, Text, StyleSheet, View, TouchableOpacity, Image, ScrollView, AsyncStorage, KeyboardAvoidingView, Platform, Picker } from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import AwesomeAlert from 'react-native-awesome-alerts';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import DatePicker from 'react-native-datepicker';
import MapView, {Marker, Callout} from 'react-native-maps';
// import RNPickerSelect from 'react-native-picker-select'
import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';
import Spinner from 'react-native-loading-spinner-overlay';
import moment from 'moment';
import Constants from 'expo-constants';



class paso1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //placeName: ''
      showAlert: false,
      nombre_solicitante:'',
      nro_documento:'',
      email:'',
      direccion_particular:'',
      barrio:'',
      telefono:'',
      date:"",
      location: null,
      title:'Por favor carga todos los datos...',
      message:'No puedes dejar los espacion en blanco...',
      spinner_loading:false,
      edad:20, 
      antiguedad_laboral_meses:0,
      edad_minima:0,
      edad_maxima:0
    };
  }
  
  nextStep = async () => {

    if(this.state.nombre_solicitante=='' || this.state.nombre_solicitante==null || 
        this.state.nro_documento=='' || this.state.nro_documento==null || 
        this.state.email=='' ||  this.state.email==null || 
        this.state.direccion_particular=='' || this.state.direccion_particular==null || 
        this.state.barrio=='' ||  this.state.barrio==null || 
        this.state.date=='' ||  this.state.date==null || 
        this.state.estado_civil=='' ||  this.state.estado_civil==null || 
        this.state.telefono=='' || this.state.telefono==null){
        this.setState({title:'Por favor carga todos los datos...',
        message:'No puedes dejar los espacion en blanco...'})

      this.showAlert();
      return;
    }

    if(parseInt(this.state.edad)<this.state.edad_minima){
      this.setState({message:'Debe tener más de ' +  this.state.edad_minima+ ' años para poder hacer una solicitud...', title:'Atención.'})
      this.showAlert();
      return;
    }

    if(parseInt(this.state.edad)>this.state.edad_maxima){
      this.setState({message:'Debe tener menos de ' +  this.state.edad_maxima+ ' años para poder hacer una solicitud...', title:'Atención.'})
      this.showAlert();
      return;
    }

    const { next, saveState } = this.props;
    // Save state for use in other steps
    saveState({ name: "ejemplo" });

    // Go to next step
    next();
  };

  goBack() {
    const { back } = this.props;
    // Go to previous step
    back();
  }

  // _getLocationAsync = async () => {
  //   this.setState({message:'', title:'', spinner:true})
  //   let { status } = await Location.requestPermissionsAsync();

  //   if (status !== 'granted') {
  //     this.setState({
  //       message: '',
  //       spinner:false
  //     });

  //     this.setState({message:'Uff, este dispositivo no nos dio permiso para obtener su ubicación ...', title:'Atención.'})
  //     this.setState({spinner:false})
  //     this.showAlert();
  //   }

  //   let location = await Location.getCurrentPositionAsync({})
  //   .catch((error) => {

  //     this.setState({message:'Epa, por favor habilite su ubicación...', title:'Atención.'})
  //     this.setState({spinner:false})
  //     this.showAlert();

  //   });
  //   this.setState({spinner:false});
  // };

  async componentDidMount(){
    await this.setState({
      parametros: [],
    }, async ()=>{ 
      // locales de la web
      var response_parametros = await fetch('http://18.221.77.253/admin/parametros/getParametros')
      var responseJson_parametros = await response_parametros.json()
      this.setState({
        parametros: responseJson_parametros
      }, ()=>{
        this.state.parametros.map((x) => {
          if(x.idparametros==4){
            this.setState({antiguedad_laboral_meses:x.valor})
            this.antiguedad(x.valor)
          }
          if(x.idparametros==5){
            this.setState({edad_minima:x.valor})
          }
          if(x.idparametros==6){
            this.setState({edad_maxima:x.valor})
          }
        });
      })
    });
     /*AsyncStorage.getAllKeys((err, keys) => {
      AsyncStorage.multiGet(keys, (err, stores) => {
        stores.map(async(result, i, store) => {
          // get at each store's key/value so you can work with it

          await AsyncStorage.removeItem(store[i][0])
        });
      });
    });*/

    // if (Platform.OS === 'android' && !Constants.isDevice) {
    //   await this._getLocationAsync();
      
    // } else {
    //  // this.fetchData();
    //   await this._getLocationAsync();
    // }
    
      this.setState({
        nombre_solicitante: await AsyncStorage.getItem('nombre_solicitante'),
        nro_documento: await AsyncStorage.getItem('nro_documento'),
        email: await AsyncStorage.getItem('email'),
        direccion_particular: await AsyncStorage.getItem('direccion_particular'),
        barrio: await AsyncStorage.getItem('barrio'),
        telefono: await AsyncStorage.getItem('telefono'),
        date: await AsyncStorage.getItem('fecha_nacimiento'),
        estado_civil: await AsyncStorage.getItem('estado_civil')
      })
  }

  antiguedad = async (valor) => {
    
    await AsyncStorage.setItem('antiguedad_laboral_meses', valor)
  }

  handleInputChange = async (inputName, inputValue) => {
    this.setState(state => ({
      ...state, 
      [inputName]: inputValue
    }));
    if(inputName!='email' && inputName!='direccion_particular'){
      inputValue = inputValue.replace("Gs ", "");
      inputValue = inputValue.replace(".", "");
      inputValue = inputValue.replace(".", "");
    }
    
    await AsyncStorage.setItem(inputName, inputValue)
  }

  showAlert = () => {
    this.setState({
      showAlert: true
    });
  };

  hideAlert = () => {
    this.setState({
      showAlert: false
    });
  };

  async date (date){
    this.setState({date})


    var current_year = new Date().getFullYear();
    this.setState({edad:current_year-this.state.date.split('/')[2].trim()})

    await AsyncStorage.setItem('fecha_nacimiento', date)
     
  }

  async snipper(inputName, inputValue){
    this.setState(state => ({
      ...state, 
      [inputName]: inputValue
    }));
    await AsyncStorage.setItem(inputName, inputValue)
  }
  

  render() {
    const {showAlert} = this.state;
    return (
      <KeyboardAvoidingView
      behavior={Platform.OS == "ios" ? "padding" : "height"}
      style={styles.container}
    >
       <Spinner
          visible={this.state.spinner}
          textContent={'Cargando...'}
          textStyle={styles.spinnerTextStyle}
        />
          
        <ScrollView contentContainerStyle={styles.Main}>

       
              <View style={styles.Cabecera} >
                  <Image source={require("../img/logo-2.png")} style={styles.Logo}/>
                  <Image source={require("../img/figura.png")} style={styles.Figura}/>
              </View>
              <View>
                
              <Text style={styles.TxtTitulo}>Datos del solicitante:</Text>
                <TextInput value={this.state.nombre_solicitante} name="nombre_solicitante" onChangeText={value => this.handleInputChange('nombre_solicitante', value)} placeholderTextColor="#333" placeholder='Nombre Completo' style={styles.formulario} />
                <TextInput value={this.state.nro_documento} name="nro_documento" onChangeText={value => this.handleInputChange('nro_documento', value)} placeholderTextColor="#333" placeholder='Nro. Documento'  style={styles.formulario} />
                <DatePicker
                style={{width: 'auto',
                  borderWidth: 0.5,
                  borderRadius:15,
                  borderColor: '#FFFFFF',

                  backgroundColor:'#FFFFFF',
                  marginTop:10,
                  marginBottom:10,
                  paddingLeft:10
                }}
                date={this.state.date}
                mode="date"
                placeholder="Fecha de nacimiento"
                placeholderTextColor="#FFFFFF"
                format="DD/MM/YYYY"
                minDate="01-01-1920"
                maxDate="01-01-2020"
                confirmBtnText="Confirmar"
                cancelBtnText="Cancelar"
                customStyles={{
                  
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0
                  },
                  dateInput: {
                    marginLeft: 6,
                    borderWidth: 0,
                    width:'auto'
                  }
                 
                }}
                onDateChange={(date) => {this.date(date)}}

              />
              {/* <RNPickerSelect
                  placeholder={{
                      label: 'Estado civil',
                      
                  }}
                  value={this.state.estado_civil}
                  onValueChange={(value) => this.snipper('estado_civil', value)}
                  items={[
                      { label: 'Soltero/a', value: 'Soltero' },
                      { label: 'Casado/a', value: 'Casado' },
                      { label: 'Casado/a con separación de bienes', value: 'Casado2' },
                      { label: 'Divorciado/a', value: 'Divorciado' },
                      { label: 'Viudo/a', value: 'Viudo' },
                  ]}
              /> */}
              <View style={{borderRadius: 15, borderWidth: 1, borderColor: '#fff', overflow: 'hidden', marginTop:10, marginBottom:10}}>

              <Picker 
                selectedValue={this.state.estado_civil}
                onValueChange={(value) => this.snipper('estado_civil', value)}
                style={{ width: 'auto', color:'#fff', height: 35 }}
                mode="dropdown" >
                <Picker.Item label="Estado Civil" value="" />
                <Picker.Item label="Soltero/a" value="Soltero" />
                <Picker.Item label="Casado/a" value="Casado" />
                <Picker.Item label="Casado/a con separación de bienes" value="Casado2" />
                <Picker.Item label="Divorciado/a" value="Divorciado" />
                <Picker.Item label="Viudo/a" value="Viudo" />
              </Picker>
              </View>
                <TextInput keyboardType='email-address' type='email' value={this.state.email} name="email" onChangeText={value => this.handleInputChange('email', value)} placeholderTextColor="#333" placeholder='Email' style={styles.formulario} />
                <TextInput value={this.state.direccion_particular}  name="direccion_particular" onChangeText={value => this.handleInputChange('direccion_particular', value)} placeholderTextColor="#333" placeholder='Dirección Particular' style={styles.formulario} />
                <TextInput value={this.state.barrio}  name="barrio" onChangeText={value => this.handleInputChange('barrio', value)} placeholderTextColor="#333" placeholder='Barrio' style={styles.formulario} />
                <TextInput value={this.state.telefono} name="telefono" onChangeText={value => this.handleInputChange('telefono', value)} placeholderTextColor="#333" placeholder='Teléfono Particular' style={styles.formulario} />
                
               
              </View>
              <View style={{marginBottom:15}}>
                  <TouchableOpacity style={styles.BtnNext}  onPress={this.nextStep}>
                    <Text style={styles.TxtNext}>Siguiente</Text>
                  </TouchableOpacity>

              </View>
        </ScrollView>
        <AwesomeAlert
            show={showAlert}
            showProgress={false}
            title={this.state.title}
            message={this.state.message}
            closeOnTouchOutside={true}
            closeOnHardwareBackPress={false}
            showCancelButton={false}
            showConfirmButton={true}
        //    cancelText="No, cancel"
            confirmText="OK!"
            confirmButtonColor="#DD6B55"
            onCancelPressed={() => {
              this.hideAlert();
            }}
            onConfirmPressed={() => {
              this.hideAlert();
            }}
          />

        </KeyboardAvoidingView>

    );
  }
}

export default paso1;
const styles = StyleSheet.create({
  Main: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#302D5D'
  },
  container: {
    flex: 1
  },
  Cabecera:{
    width: wp('100%'),
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
  },
  Logo: {
    marginTop:50,
    width: 250,
    height:50,
    resizeMode:"contain"
  },
  Figura: {
    width: wp('100%'),
    resizeMode:"contain",
    marginBottom: -5,
    marginTop:10
  },
  BtnNext:{
    width: wp('60%'),
    borderWidth: 0.5,
    borderRadius:15,
    padding:10,
    borderColor: '#FFFFFF',
    marginTop:20,
    marginBottom:10
  },
  TxtNext:{
    textAlign: "center",
    fontSize:15,
    color:"#FFFFFF",
   
  },
  TxtTitulo:{
    textAlign: "center",
    fontSize:22,
    color:"#FFFFFF",
   
  },
  formulario:{
    width: wp('90%'),
    height:40,
    borderWidth: 0.5,
    borderRadius:15,
    borderColor: '#FFFFFF',
    backgroundColor:'#FFFFFF',
    marginTop:10,
    marginBottom:10,
    paddingLeft:10,
    color:'#000'
  }
 
});