import React, { Component } from 'react';
import {TouchableOpacity,Image, StyleSheet, View, Text } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
// import all basic components
export default class Inicio extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerShown: false,
        };
      };

    render() {
        return (
            <View style={styles.MainContainer}>
                 <Image source={require("../img/logo_inicio.png")} style={styles.Logo}/>
                 <Text style={styles.textRequisitos}>Fotocopia de cédula de identidad.</Text>
                 <Text style={styles.textRequisitos}>Justificante de ingresos (certificado de trabajo, o las ultimas 3 declaraciones de IVA).</Text>
                 <Text style={styles.textRequisitos}>Fotocopia de último pago de servicios (ANDE, COPACO o ESSAP).</Text>
                 <TouchableOpacity onPress={() => this.props.navigation.navigate('Inicio')} style={styles.BtnSolicitar} >
                     <Text style={styles.TxtBtn} >Volver</Text>
                 </TouchableOpacity> 
                 
            </View>
         )
    }
}

const styles = StyleSheet.create({
    MainContainer: {
      flex: 1,
      alignItems: 'center',
      padding:15
    },
    Logo:{
        marginTop:100,
        width: 200,
        height:200,
        resizeMode:"contain"
    },
    BtnSolicitar:{
        width: wp('70%'),
        backgroundColor: '#FFF200',
        padding:15,
        borderRadius:15,
        position: "absolute",
        bottom:80,
        shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 3,
},
shadowOpacity: 0.27,
shadowRadius: 4.65,

elevation: 6,
        
    },
    TxtBtn:{
        textAlign: "center",
        fontSize:20,
        fontWeight:"bold"
    },
    textRequisitos:{
        textAlign:'center',
        color:'#302D5D',
        fontSize:15,
        fontWeight:'bold',
        marginTop:10
    }

});