import React, { Component } from 'react';
import {ScrollView, StyleSheet, } from 'react-native';
import AnimatedMultistep from 'react-native-animated-multistep'


import Paso1 from "./Paso1";
import Paso2 from "./Paso2";
import Paso3 from "./Paso3";
import Paso4 from "./Paso4";


const allSteps = [
    { name: "paso 1", component: Paso1 },
    { name: "paso 2", component: Paso2 },
    { name: "paso 3", component: Paso3 },
    { name: "paso 4", component: Paso4 }
  ];

export default class Inicio extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
          headerShown: false,
        };
      };
      onNext = () => {
        console.log("Next");
      };
      onBack = () => {
        console.log("Back");
      };

        finish = finalState => {
            console.log(finalState);
        };

    render() {
        return (
          
         
            <ScrollView style={{ flex: 1,backgroundColor: '#302D5D' }}>
                <AnimatedMultistep
                steps={allSteps}
                onFinish={this.finish}
                onBack={this.onBack}
                onNext={this.onNext}
                comeInOnNext="bounceInUp"
                OutOnNext="bounceOutDown"
                comeInOnBack="bounceInDown"
                OutOnBack="bounceOutUp"
                />
            </ScrollView>
       
         )
    }
}

const styles = StyleSheet.create({


});