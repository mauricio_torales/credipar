import React, { Component } from 'react';
import {ScrollView,Image, StyleSheet, View, Text, AsyncStorage, Linking } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Emoji from 'react-native-emoji';
// import all basic components
export default class Inicio extends Component {
  constructor(props) {
    super(props);
    this.state = {
      estadoPedido :''
    };

    
  }

  

    static navigationOptions = ({ navigation }) => {
        return {
          headerShown: false,
        };
      };

      async componentDidMount(){
        AsyncStorage.getAllKeys((err, keys) => {
         AsyncStorage.multiGet(keys, (err, stores) => {
           stores.map(async(result, i, store) => {
             // get at each store's key/value so you can work with it
   
             await AsyncStorage.removeItem(store[i][0])
           });
         });
       });

       this.setState({
        estadoPedido: await AsyncStorage.getItem('estadoPedido')
      })
      
      }

    render() {
        return (
            <ScrollView>
               <View style={styles.Cabecera} >
                    <Image source={require("../img/logo-2.png")} style={styles.Logo}/>
                    <Image source={require("../img/figura.png")} style={styles.Figura}/>
              </View>
              <View>

                  {this.state.estadoPedido == 'P'? <Text style={styles.Aprobado} >Crédito Pre Aprobado.<Emoji name="tada" style={{fontSize: 40}} /></Text>
                  : <Text style={styles.Aprobado} >Un oficial de créditos se pondra en contacto con usted.<Emoji name="warning" style={{fontSize: 40}} />
                  </Text> }
                  
                  {this.state.estadoPedido == 'P'? <Text style={styles.Verificacion}>*Sujeto a verificación.</Text>: <Text style={styles.Verificacion}>*Nos contactaremos con usted para un mejor asesoramiento.</Text> }

                  <Text style={styles.Verifi}>Puedes comunicarse a Credipar SA a los Tel. : <Text style={{fontWeight:"bold"}} onPress={() => Linking.openURL(`tel:071201840`)}>(071) 201840</Text> / <Text style={{fontWeight:"bold"}} onPress={() => Linking.openURL(`tel:0985700750`)}> (0985) 700750 </Text></Text>
              </View>
            </ScrollView>
         )
    }
}

const styles = StyleSheet.create({
    Main: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#FFF'
      },
      Cabecera:{
        width: wp('100%'),
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
      },
      Logo: {
        marginTop:10,
        width: 250,
        height:50,
        resizeMode:"contain"
      },
      Figura: {
        width: wp('100%'),
        resizeMode:"contain",
        marginBottom: -5,
        marginTop:10
      },
    Logo:{
        marginTop:100,
        width: 200,
        height:200,
        resizeMode:"contain"
    },
    Aprobado:{
        color: '#FFF',
        fontSize:20,
        textAlign:"center",
        fontWeight:'bold',
        marginTop:25
    },
    Verificacion:{
        color: '#FFF',
        textAlign:"center",
        marginTop: 20
    },
    Verifi:{
        color: '#FFF',
        textAlign:"center",
        marginTop: 5
    }
   

});